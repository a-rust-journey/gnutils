use std::{process::ExitCode, io::BufRead, io::BufWriter, io::Write};

use anyhow::Result;
use clap::Parser;
use thiserror::Error;

const STD_INPUT: &str = "-";

#[derive(Parser, Debug)]
#[command(name = "GNUtils - cat")]
#[command(author = "guuus")]
#[command(version = env!("CARGO_PKG_VERSION"))]
#[command(
    about = "Concatenate FILE(s) to standard output.",
    )]
struct Arguments {
    
    /// number nonempty output lines, overrides -n
    #[arg(short = 'b', long)]
    number_nonblank: bool,

    /// display $ at end of each line
    #[arg(short = 'E', long)]
    show_ends: bool,

    /// number all output lines
    #[arg(short = 'n', long)]
    number: bool,
    
    /// suppress repeated empty output lines
    #[arg(short, long)]
    squeeze_blank: bool,

    /// display TAB characters as ^I
    #[arg(short = 'T', long)]
    show_tabs: bool,

    /// use ^ and M- notation, except for LFD and TAB
    #[arg(short = 'v', long)]
    show_nonprinting: bool,

    /// equivalent to -vET
    #[arg(short = 'A', long)]
    show_all: bool,

    /// equivalent to -vE
    #[arg(short = 'e')]
    show_nonprinting_and_ends: bool,

    /// equivalent to -vT
    #[arg(short = 't')]
    show_nonprinting_and_tabs: bool,

    /// With no FILE, or when FILE is -, read standard input.
    #[arg(default_values_t = [String::from("-")])]
    files: Vec<String>,
}

#[derive(Debug, Error)]
enum CatError {
    #[error("Unknwon")]
    Unknown,
    /// Wrapper around `io::Error`
    #[error("{0}")]
    Io(#[from] std::io::Error),
}

type CatResult<T> = Result<T, CatError>;
type Res = CatResult<()>;

fn parse() -> Arguments {
    let mut args = Arguments::parse();

    if args.show_all {
        args.show_nonprinting = true;
        args.show_ends = true;
        args.show_tabs = true;
    }

    if args.show_nonprinting_and_ends {
        args.show_nonprinting = true;
        args.show_ends = true;
    }

    if args.show_nonprinting_and_tabs {
        args.show_nonprinting = true;
        args.show_tabs = true;
    }

    args
}

enum File {
    StdInput,
    Path(String)
}

impl File {
    fn display(&self, args: &Arguments) -> Res {
        let mut input = self.get_input()?;
        let mut buffer = Vec::new();
        let mut line_number: usize = 1;
        let mut empty_line_count:usize = 0;
        let mut output = BufWriter::new(std::io::stdout().lock());

        // change readline to read or read_until to get a bytes array
        while input.read_until(b'\n', &mut buffer)? > 0 {
            buffer.pop();

            // -s
            if args.squeeze_blank && buffer.is_empty() {
                empty_line_count += 1;
                if empty_line_count > 1 {
                    continue;
                }
            } else if args.squeeze_blank {
                empty_line_count = 0;
            }

            // -b
            if args.number_nonblank {
                if !buffer.is_empty() {
                    write!(output, "{:>6}\t", line_number)?;
                    line_number += 1;
                }
            // -n
            } else if args.number {
                write!(output, "{:>6}\t", line_number)?;
                line_number += 1;
            }

            // -E
            if args.show_ends { buffer.push(b'$') }
            // -t
            if args.show_tabs { 
                while let Some(tab_index) = buffer.iter().position(|byte| byte == &b'\t') {
                    buffer.remove(tab_index);
                    buffer.insert(tab_index, b'^');
                    buffer.insert(tab_index+1, b'I');
                }
                
            }
            //-v
            if args.show_nonprinting {
                for byte in &buffer {
                    output.write(non_printable_equivalent(*byte).as_slice())?;
                }
            } else {
                output.write(buffer.as_slice())?;
            }

            writeln!(output, "")?;
            buffer.clear();
        };
        Ok(())
    }

    fn get_input(&self) -> CatResult<Box<dyn std::io::BufRead>> {
        if let File::StdInput = self {
            return self.open_stdin()
        } else {
            return self.open_file()
        }
    }

    fn open_stdin(&self) -> CatResult<Box<dyn std::io::BufRead>> {
        Ok(Box::new(std::io::stdin().lock()))
    }

    fn open_file(&self) -> CatResult<Box<dyn std::io::BufRead>> {
        if let File::Path(path) = self {
            let file = std::fs::File::open(path)?;
            let reader = std::io::BufReader::new(file);

            return Ok(Box::new(reader))
        }
        Err(CatError::Unknown)
    }
}

fn non_printable_equivalent(input: u8) -> Vec<u8> {
    match input {
        0..=8 | 10..=31 => vec![b'^', input + 64],
        9 | 32..=126 => vec![input],
        127 => vec![b'^', b'?'],
        128..=159 => vec![b'M', b'-', b'^', input - 64],
        160..=254 => vec![b'M', b'-', input - 128],
        _ => vec![b'M', b'-', b'^', b'?'],
    }
}

impl Into<File> for &String {
    fn into(self) -> File {
        match self.as_str() {
            STD_INPUT => File::StdInput,
            s => File::Path(String::from(s))
        }
    }
}

fn handle_error(res: Res) -> bool {
    if let Ok(()) = res {
        return false
    }

    if let Err(error) = res {
        eprintln!("{}", error);
    }
    true
}

fn main() -> ExitCode {
    let args = parse();

    let files: Vec<File> = args.files.iter().map(|file| file.into()).collect();

    let mut error_ocurred = false;
    
    for file in files {
        if handle_error(file.display(&args)) {
            error_ocurred = true;
        }
    }

    if error_ocurred {
        return ExitCode::from(130);
    }

    ExitCode::SUCCESS
}
