use std::{iter::Peekable, str::Chars};

use clap::Parser;

#[derive(Parser)]
#[command(name = "GNUtils - echo")]
#[command(author = "guuus")]
#[command(version = env!("CARGO_PKG_VERSION"))]
#[command(about = "Echo the STRING(s) to standard output.")]
struct Arguments {

    /// do not output the trailing newline
    #[arg(short, default_value_t = false)]
    no_new_line: bool,

    /// enable interpretation of backslash escapes
    #[arg(short = 'e', default_value_t = false)]
    interpret: bool,

    /// disable interpretation of backslash escapes (default)
    /// 
    /// If -e is in effect, the following sequences are recognized:
    ///
    /// \\
    ///     backslash 
    /// 
    /// \a
    ///     alert (BEL) 
    /// 
    /// \b
    ///     backspace 
    /// 
    /// \c
    ///     produce no further output 
    /// 
    /// \e
    ///     escape 
    /// 
    /// \f
    ///     form feed 
    /// 
    /// \n
    ///     new line 
    /// 
    /// \r
    ///     carriage return 
    /// 
    /// \t
    ///     horizontal tab 
    /// 
    /// \v
    ///     vertical tab 
    /// 
    /// \0NNN
    ///     byte with octal value NNN (1 to 3 digits) 
    /// 
    /// \xHH
    ///     byte with hexadecimal value HH (1 to 2 digits)
    #[arg(short = 'E', default_value_t = true)]
    interpret_default: bool,

    #[arg(name = "STRING")]
    strings: Vec<String>
}

fn main() {
    let arguments = Arguments::parse();

    display(&arguments.strings, arguments.interpret);

    if !arguments.no_new_line {
        println!()
    }
}

fn display(strings: &[String], interpret: bool) {
    let mut first = true;
    for string in strings {
        if !first {
            print!(" ")
        }
        first = false;
        let should_stop = display_arg(string, interpret);
        if should_stop {break}
    }
}

fn display_arg(string: &String, interpret: bool) -> bool {
    if !interpret {
        print!("{string}");
        false
    } else {
        display_escaped_arg(string)
    }
}

fn display_escaped_arg(string: &String) -> bool{
    let mut should_stop = false;

    let mut buffer = ['\\'; 2];

    let mut iter = string.chars().peekable();
    while let Some(mut c) = iter.next() {
        let mut start = 1;

        if c == '\\' {
            if let Some(next) = iter.next() {
                c = match next {
                    '\\' => '\\',
                    'a' => '\x07',
                    'b' => '\x08',
                    'c' => {
                        should_stop = true;
                        break;
                    }
                    'e' => '\x1b',
                    'f' => '\x0c',
                    'n' => '\n',
                    'r' => '\r',
                    't' => '\t',
                    'v' => '\x0b',
                    'x' => parse_code(&mut iter, 16, 2, 4).unwrap_or_else(|| {
                        start = 0;
                        next
                    }),
                    '0' => parse_code(&mut iter, 8, 3, 3).unwrap_or('\0'),
                    _ => {
                        start = 0;
                        next
                    }
                };
            }
        }

        buffer[1] = c;

        // because printing char slices is apparently not available in the standard library
        for ch in &buffer[start..] {
            print!("{ch}");
        }
    }
    should_stop
}

fn parse_code(
    input: &mut Peekable<Chars>,
    base: u32,
    max_digits: u32,
    bits_per_digit: u32,
) -> Option<char> {
    let mut ret = 0x8000_0000;
    for _ in 0..max_digits {
        match input.peek().and_then(|c| c.to_digit(base)) {
            Some(n) => ret = (ret << bits_per_digit) | n,
            None => break,
        }
        input.next();
    }
    std::char::from_u32(ret)
}